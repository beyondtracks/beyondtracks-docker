FROM debian:sid-slim

RUN apt-get -y update && apt-get -y install curl gnupg openssh-client rsync && rm -rf /var/lib/apt/lists/*
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && apt-get -y update && apt-get -y install nodejs yarn && rm -rf /var/lib/apt/lists/*
RUN apt-get -y update && apt-get -y install time make webp postgresql postgis postgresql-contrib osm2pgsql gdal-bin wget osmium-tool python-pip pgtap awscli s3cmd rclone parallel git unzip zip pngcrush otb-bin jq csvkit miller && rm -rf /var/lib/apt/lists/*
RUN pip install mapboxcli && rm -rf /root/.cache/pip
RUN npm install -g @mapbox/geojsonhint
RUN apt-get -y update && apt-get -y install build-essential libsqlite3-dev zlib1g-dev && git clone --depth=1 https://github.com/mapbox/tippecanoe.git && cd tippecanoe && make -j && make install && cd .. && rm -rf tippecanoe && apt-get -y purge --auto-remove build-essential libsqlite3-dev zlib1g-dev && apt-get clean && rm -r /var/lib/apt/lists/*
RUN pip install rio-rgbify && sed -i "s/init='epsg:3857'/epsg:3857/" /usr/local/lib/python2.7/dist-packages/rio_rgbify/mbtiler.py && rm -rf /root/.cache/pip
RUN pip install mbutil && rm -rf /root/.cache/pip
