# beyondtracks-docker

Using GitLab CI to build a Dockerfile, which in turn is used to bootstrap the GitLab CI build process for beyondtracks by providing a base image with dependencies pre-installed.

See the rationale at https://tech.beyondtracks.com/posts/speeding-up-gitlab-ci-with-docker/
